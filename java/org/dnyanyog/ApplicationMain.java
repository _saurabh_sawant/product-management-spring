package org.dnyanyog;

import org.dnyanyog.data.Product;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;

public class ApplicationMain {
	
	public static void main(String[] args) {
		
		ConfigurableApplicationContext context = SpringApplication.run(ApplicationMain.class, args);
		
		//Object => Bean
		
		Product product = context.getBean(Product.class);
		
		product.productName = "Pen";
		product.price=(double)5;
		product.quantity=100;
		
		product.category.name="Stationary";
		
		product.variations.variationList.add("Red Color");
		product.variations.variationList.add("Blue Color");
		product.variations.variationList.add("Black Color");
		
		product.printProductDetails();
	}
}
